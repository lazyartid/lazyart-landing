# Main file

The main file (usually labelled `style.scss`), The second files was print style purposes (usually labelled `print.scss`) and the other file was theme styling files (usually labelled `style-{theme_name}.scss`). Those file should be the only Sass file from the whole code base not to begin with an underscore. This file should not contain anything but `@import` and comments.